# Thunder Download Manager

一个极速的Chrome下载管理器!!!

![preview.png](preview.png)

## Chrome 商店
[https://chrome.google.com/webstore/detail/thunder-download-manager/nllgadppmkoamdlocamdnmimkehhojai](https://chrome.google.com/webstore/detail/thunder-download-manager/nllgadppmkoamdlocamdnmimkehhojai)

## 国际化

见 `src/crx/_locales`

## 支持

欢迎pr和issue

## TODO

- [ ] 多线程下载
- [ ] 声音提醒
- [ ] 新建下载任务时动画效果
- [x] 自定义添加新下载任务
- [x] 文件拖拽
- [x] 显示`允许不安全内容`的弹窗
- [x] 统一使用`chrome.i18n`
- [ ] 抓取当前标签页内的媒体文件
- [ ] 更多...

## QQ群

773161099