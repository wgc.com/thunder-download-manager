# Thunder Download Manager

A super fast download manager for chrome!!!

![preview.png](preview.png)

## Chrome Store
[https://chrome.google.com/webstore/detail/thunder-download-manager/nllgadppmkoamdlocamdnmimkehhojai](https://chrome.google.com/webstore/detail/thunder-download-manager/nllgadppmkoamdlocamdnmimkehhojai)

## Localization

See `src/crx/_locales`

## Support

pr and issue are welcome!

## TODO

- [ ] Multi-thread download
- [ ] Notification sound
- [ ] New task created animation 
- [x] Custom create new task
- [x] Drag downloaded file
- [x] Show `accept danger` dialog
- [x] Use `chrome.i18n`
- [ ] Grab media files in current tab
- [ ] more...